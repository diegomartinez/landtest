
local fs = love.filesystem
fs.setRequirePath("lib/?.lua;lib/?/init.lua;"..fs.getRequirePath())

local getinfo = debug.getinfo
function print(...)
	local n, t = select("#", ...), { ... }
	for i = 1, n do
		t[i] = tostring(t[i])
	end
	local info = getinfo(2)
	local loc = ((info and info.source
			and info.source:match("^@(.*)") or info.source or "?")
			..":"..(info and info.currentline or "?"))
	return io.stderr:write(loc, ": ", table.concat(t, " "), "\n")
end

require "landtest.main"
