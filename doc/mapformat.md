
# Map formats

A map consists of a directory with (at least) one file named `info` containing
basic information about the map.

## Map information

The `info` file is (the representation of) a Lua table with the following
fields:

* `backend`:
  Database backend used by the map.

  This may be the name of any module under `lib/landtest/map/db/`. At the time
  of writing, this may be any of `lua`, `sqlite3`, or `null`.

* `vars`:
  Global per-map variables.

  Scripts may set fields in this table with the `landtest.map.setvar` function.

## Map data

There are two basic objects: hashes, and block data.

The map is divided in *blocks* of *tiles*. Tiles are the building blocks of the
world. Blocks are 32 by 32 blocks of tiles. The max map size is 65536 by 65536
blocks, thus the maximum map size is 2097152 by 2097152 tiles.

Hashes uniquely identify blocks. They are encoded as two 16-bit signed
integers into a single 32-bit integer. The first integer indicates the block's
X position, and the second the Y position, both in block (32 tile) units.

The format and location of the container file(s) depends on the backend used.

### Lua backend

The `lua` backend stores map data in a file named `data`. It is a `msgpack`-ed
Lua table with block hash as keys, and block data as values. The whole file is
compressed using the DEFLATE compression scheme.

### SQLite3 backend

This backend is not implemented yet.
