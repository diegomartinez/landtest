
# LandTest

LandTest is a tile-based 2D sandbox game in the spirit of Terraria, using the
[LÖVE][love2d] game engine.

## License

This program is free software released under a permissive license. You can
use it both in Free and closed source software. See `LICENSE.md` for details.

## Controls

## Configuration

The configuration is stored in a file named `landtest.conf` in either the save
directory, or the directory where the game executable resides, with the latter
having preference over the former.

The format is a regular Lua script. It may either set options by setting
variables, or return a table of options.

The available options are as follows:

* `map = "NAME"`:
  Set map name.

  The default is `main`.

  Maps are saved in the save directory (see `--print-save-dir` below).

* `logfile = "PATH"`:
  Set path to log file.

  The default is `@/landtest.log`.

  The path may be relative or absolute. If it begins with `@`, the save
  directory is prepended.

* `width = NUMBER`:
  Set width of the game window.

  The default is 800.

* `height = NUMBER`:
  Set height of the game window.

  The default is 600.

* `maximized = BOOLEAN`:
  Specify whether or not to maximize the game window on start.

  The default is false.

## Command line options

The game accepts options via the command line. These options override those
set in the configuration file.

* `-h` / `--help`:
  Show a summary of options and exit successfully.

* `--version`:
  Print version information and exit successfully.

* `--config PATH`:
  Set path to configuration file.

* `--open-save-dir`:
  Open the save directory in the default file manager and exit successfully.

* `--print-save-dir`:
  Print path to save directory and exit successfully.

* `--map NAME`:
  Equivalent to `map = "NAME"`.

* `--log-file PATH`:
  Equivalent to `logfile = "PATH"`.

* `--width NUMBER`:
  Equivalent to `width = NUMBER`.

* `--height NUMBER`:
  Equivalent to `height = NUMBER`.

* `--maximized` / `--no-maximized`:
  Equivalent to `maximized = true` and `maximized = false`, respectively.

The program exits with status code 0 if no errors occurred or if one of
`--help`, `--version`, or `--print-save-dir` is given. If an error occurs
during initialization or if `--map` is given and there is any problem loading
the map, it exits with status code 1. If there's an error in the command line
options, it exits with status code -1.

[love2d]: http://love2d.org "LÖVE"
