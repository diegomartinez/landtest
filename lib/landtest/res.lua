
---
-- Resource manager.
--
-- @module landtest.res

local filesystem = love.filesystem
local stringex = require "stringex"
local minser = require "minser"

local res = { }

local function makegetter(loader)
	assert(loader)
	local cache = setmetatable({ }, { __mode="kv" })
	return function(...)
		local hash = stringex.concat("\0", ...)
		local x = cache[hash]
		if not x then
			local ok, err
			ok, x, err = pcall(loader, ...)
			if not ok then return nil, x end
			if not x then return nil, err end
			cache[hash] = x
		end
		return x
	end
end

---
-- TODO: Doc.
--
-- @function getimage
-- @tparam string filename
-- @treturn love.graphics.Image
res.getimage = makegetter(love.graphics.newImage)

---
-- TODO: Doc.
--
-- @function getsound
-- @tparam string filename
-- @treturn love.audio.Source
res.getsound = makegetter(love.audio.newSource)

---
-- TODO: Doc.
--
-- @function getmusic
-- @tparam string filename
-- @treturn love.audio.SoundSource
res.getmusic = makegetter(love.audio.newSource)

local function readdata(filename, mode)
	local f, err, data
	f, err = filesystem.newFile(filename, mode)
	if not f then
		return nil, err
	end
	data, err = f:read()
	if not data then
		return nil, err
	end
	return data
end

---
-- TODO: Doc.
--
-- @function getdata
-- @tparam string filename
-- @treturn string
res.getdata = makegetter(function(filename)
	local data, err = readdata(filename, "r")
	if not data then
		return nil, err
	end
	return data
end)

---
-- @function gettable
-- @tparam string filename
-- @treturn table
res.gettable = makegetter(function(filename)
	local data, err = readdata(filename, "r")
	if not data then
		return nil, err
	end
	local function bail(ok, ...)
		if not ok then
			return nil, ...
		end
		return ...
	end
	return bail(minser.load(data))
end)

---
-- @function getcode
-- @tparam string filename
-- @tparam ?table env
-- @treturn function
res.getcode = makegetter(function(filename, env)
	local data, err = readdata(filename, "r")
	if not data then
		return nil, err
	end
	if data:byte(1) == 27 then
		return nil, "refused to load bytecode"
	end
	local func = loadstring(data)
	setfenv(func, env or { })
	return func
end)

for _, filename in ipairs(filesystem.getDirectoryItems("/packs")) do
	filesystem.mount("/packs/"..filename, "/")
end

return res
