
---
-- TODO: Doc.
--
-- @module landtest.map.utils

local utils = { }

local BS = 32
local BPR = 65536
local MS = BS*BPR

---
-- TODO: Doc.
--
-- @tfield number MAPSIZE
utils.MAPSIZE = MS

---
-- TODO: Doc.
--
-- @tfield number BLOCKSIZE
utils.BLOCKSIZE = BS

---
-- TODO: Doc.
--
-- @tfield number BLOCKSIZE
utils.BLOCKSPERROW = BPR

---
-- TODO: Doc.
--
-- @tparam number bx
-- @tparam number by
-- @treturn number|nil hash
-- @treturn nil|string error
function utils.postohash(bx, by)
	bx, by = bx+0x8000, by+0x8000
	assert(bx>=0 and bx<0x10000 and by>=0 and by<0x10000, "out of bounds")
	return by*0x10000 + bx
end

---
-- TODO: Doc.
--
-- @tparam number hash
-- @treturn number bx
-- @treturn number by
function utils.hashtopos(hash)
	assert(hash>=0 and hash<=0xFFFFFFFF, "invalid hash")
	local bx, by = math.floor(hash) % 0x10000, math.floor(hash/0x10000)
	return bx-0x8000, by-0x8000
end

return utils
