
---
-- TODO: Doc.
--
-- @module landtest.map.gen

local lmath = love.math
local utils = require "landtest.map.utils"

local gen = { }

local BS = utils.BLOCKSIZE

---
-- TODO: Doc.
--
-- @tparam table block
-- @tparam number bx
-- @tparam number by
function gen.generate(block, bx, by)
	for ly = 0, BS-1 do
		for lx = 0, BS-1 do
			local absx, absy = bx*BS+lx, by*BS+ly
			local ht = lmath.noise((bx*BS+lx)/256, 0)-.5
			local lcave = lmath.noise( absx/128, absy/128)
			local mcave = lmath.noise( absx/32,  absy/32)
			local scave = lmath.noise( absx/8,   absy/8)
			local stone = (lmath.noise(absx/8,   absy/8)
					*math.log(absy, 10)/2)
			local id = "air"
			if absy > ht*64 then
				if lcave*mcave*scave < .1 then
					id = stone > .5 and "stone" or "dirt"
				end
			end
			block[ly*BS+lx+1] = { id=id }
		end
	end
end

return gen
