
---
-- Database backend that does nothing.
--
-- This module serves three purposes. First, it documents the interface for
-- backend modules; second, it functions as a template for new backends; and
-- third, it is useful for testing temporary maps that should not be saved
-- at all.
--
-- @module landtest.map.db.null

local backend = { }

---
-- TODO: Doc.
--
-- @tparam string dbname
function backend.open(dbname)
end

---
-- TODO: Doc.
--
function backend.close()
end

---
-- TODO: Doc.
--
-- @tparam number hash
-- @treturn table block
function backend.readblock(hash)
end

---
-- TODO: Doc.
--
-- @tparam number hash
-- @tparam table block
function backend.writeblock(hash, block)
end

---
-- TODO: Doc.
--
-- @tparam number hash
function backend.deleteblock(hash)
end

return backend
