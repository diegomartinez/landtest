
---
-- Database backend using an in-memory Lua table as storage.
--
-- @module landtest.map.db.lua
-- @see landtest.map.db.null

local lmath = love.math
local filesystem = love.filesystem
local MessagePack = require "MessagePack"

local backend = { }

local dbname, dbdata, dirty

function backend.open(name)
	local data
	if filesystem.isFile("/maps/"..name.."/data") then
		data = assert(filesystem.read("/maps/"..name.."/data"))
		data = assert(lmath.decompress(data, "zlib"))
		data = MessagePack.unpack(data)
		assert(type(data) == "table")
	else
		data = { }
	end
	dbname, dbdata, dirty = name, data, nil
end

function backend.close()
	if dirty then
		local data = lmath.compress(MessagePack.pack(dbdata), "zlib", 9)
		assert(filesystem.write("/maps/"..dbname.."/data", data))
	end
	dbname, dbdata, dirty = nil, nil, nil
end

function backend.readblock(hash)
	return dbdata[hash]
end

function backend.writeblock(hash, data)
	dbdata[hash] = data
	dbdata.lastmodified = os.time()
	dirty = true
end

function backend.deleteblock(hash)
	dbdata[hash] = nil
	dbdata.lastmodified = os.time()
	dirty = true
end

return backend
