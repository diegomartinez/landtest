
---
-- Database backend using SQLite3 database files as storage.
--
-- @module landtest.map.db.sqlite3
-- @see landtest.map.db.null

local backend = { }

function backend.open(dbname)
	return error("sqlite3 backend is not implemented yet")
end

function backend.close()
end

function backend.readblock(hash)
end

function backend.writeblock(hash, block)
end

function backend.deleteblock(hash)
end

return backend
