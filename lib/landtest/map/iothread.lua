
-- This file is run in a background thread.

local timer = require "love.timer"
local minser = require "minser"

local input, output = ...
local running = true

local function send(...) -- luacheck: ignore
	return output:push(assert(minser.dump(...)))
end

local handlers = { }

function handlers.stop()
	running = nil
end

function handlers.readblock(hash, data)
end

local function handleone(ok, cmd, ...)
	assert(ok, ...)
	return handlers[cmd](...)
end

local function main()
	while running do
		handleone(minser.load(input:demand()))
		timer.sleep(0)
	end
end

return main()
