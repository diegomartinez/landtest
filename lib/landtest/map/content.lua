
---
-- TODO: Doc.
--
-- @module landtest.map.content

local logging = require "landtest.logging"

local content = { }

---
-- TODO: Doc.
--
-- @table BlockDef
-- @tfield number tileid = 0
-- @tfield boolean solid = true
-- @tfield function created = function(self)
-- @tfield function destroyed = function(self)
-- @tfield function updated = function(self, dtime)
-- @tfield function collided = function(self, other)

local blockdefs = {
	tileid = 0,
	solid = true,
	-- created = function(self) end,
	-- destroyed = function(self) end,
	-- updated = function(self) end,
	-- collided = function(self, other) end,
}

local blocks = { }
local entities = { }

---
-- TODO: Doc.
--
-- @tfield table blocks
content.blocks = blocks

---
-- TODO: Doc.
--
-- @tfield table entities
content.entities = entities

---
-- TODO: Doc.
--
-- @tparam string id
-- @tparam BlockDef def
function content.registerblock(id, def)
	logging.verbose("Registered block `"..id.."`.")
	blocks[id] = setmetatable(def, { __index=blockdefs })
end

---
-- TODO: Doc.
--
-- @tparam string id
-- @treturn BlockDef def
function content.getblock(id)
	return assert(blocks[id], "unknown block id")
end

---
-- TODO: Doc.
--
-- @tparam string id
-- @tparam klass kls
function content.registerentity(id, kls)
	logging.verbose("Registered entity `"..id.."`.")
	entities[id] = kls
end

---
-- TODO: Doc.
--
-- @tparam string id
-- @treturn TileDef def
function content.getentity(id)
	return assert(entities[id], "unknown entity id")
end

---
-- TODO: Doc.
--
-- @tparam string id
-- @tparam table params
-- @treturn landtest.map.entity.Entity entity
function content.createentity(id, params)
	return content.getentity(id)(params)
end

return content
