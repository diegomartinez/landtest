
---
-- Map-related functions.
--
-- @module landtest.map
-- @see landtest.map.utils

local filesystem = love and love.filesystem
local graphics = love and love.graphics
local MessagePack = require "MessagePack"
local minser = require "minser"
local logging = require "landtest.logging"
local res = require "landtest.res"
local gen = require "landtest.map.gen"
local content = require "landtest.map.content"
local utils = require "landtest.map.utils"

local map = { }

map.content = content
map.utils = utils
map.gen = gen

local BS = utils.BLOCKSIZE
local TIMEOUT = 5

---
-- Table representing a tile.
--
-- @table Tile
-- @tfield number id Tile ID.

local mapname
local db
local info
local blocks
local tsimage, tsimagew, tsimageh
local tilew, tileh, tilequads
local batchbase

local function readtable(filename)
	local data = assert(filesystem.read(filename))
	local function bail(ok, ...)
		assert(ok, ...)
		return ...
	end
	return bail(minser.load(data))
end

local function writetable(filename, t)
	local data = assert(minser.dump(t))
	assert(filesystem.write(filename, data))
	return true
end

local function unloadblock(hash, block)
	block.batch = nil
	block.timer = nil
	if block.dirty then
		block.dirty = nil
		db.writeblock(hash, MessagePack.pack(block))
	end
	blocks[hash] = nil
end

---
-- Open a map.
--
-- @tparam string name Map name.
-- @tparam string backend Backend name.
function map.open(name, backend)
	assert(not info, "a map is already open")
	local ts = res.gettable("data/gfx/tileset.lua")
	tsimage = res.getimage("data/gfx/"..ts.image)
	tsimagew, tsimageh = tsimage:getDimensions()
	tilew = ts.tilewidth
	tileh = ts.tileheight
	tilequads = { }
	if filesystem.isFile("/maps/"..name.."/info") then
		info = readtable("/maps/"..name.."/info")
	else
		info = { }
	end
	info.backend = info.backend or backend or "lua"
	db = require("landtest.map.db."..info.backend)
	db.open(name)
	mapname = name
	blocks = { }
	logging.info(("Map %q opened. Backend: %q"):format(name, info.backend))
end

---
-- Close the map.
--
function map.close()
	assert(info, "a map is not open yet")
	local i = info
	info = nil
	tsimage, tilequads = nil, nil
	assert(filesystem.createDirectory("/maps/"..mapname),
			"failed to create map directory")
	writetable("/maps/"..mapname.."/info", i)
	for hash, block in pairs(blocks) do
		unloadblock(hash, block)
	end
	mapname = nil
	blocks = nil
	db.close()
	logging.info("Map closed.")
end

local function gettilequad(tileid)
	local q = tilequads[tileid]
	if not q then
		if tileid == 0 then
			q = graphics.newQuad(tsimagew-tilew, tsimageh-tileh,
					tilew, tileh, tsimagew, tsimageh)
		else
			local tid = tileid - 1
			local tpr = math.floor(tsimagew/tilew)
			local tx = (tid%tpr)*tilew
			local ty = math.floor(tid/tpr)*tileh
			q = graphics.newQuad(tx, ty, tilew, tileh, tsimagew, tsimageh)
		end
		tilequads[tileid] = q
	end
	return q
end

local function createbatch(block)
	local batch = graphics.newSpriteBatch(tsimage,
			BS*BS)
	for y = 0, BS-1 do
		for x = 0, BS-1 do
			local tile = block[y*BS + x + 1]
			local id = tile and tile.id or "air"
			local tileid = content.blocks[id].tileid
			local q = gettilequad(tileid)
			local qid = batch:add(q, x*tilew, y*tileh)
			if not batchbase then
				batchbase = qid
			end
		end
	end
	block.batch = batch
end

local function getblock(bx, by, create)
	local bhash = utils.postohash(bx, by)
	local block = blocks[bhash]
	if (not block) and create then
		block = db.readblock(bhash)
		if block then
			block = MessagePack.unpack(block)
			assert(type(block) == "table")
		else
			block = { }
			gen.generate(block, bx, by)
		end
		createbatch(block)
		blocks[bhash] = block
	end
	return block
end

---
-- Get a tile table.
--
-- @tparam number x Absolute X, in tile units.
-- @tparam number y Absolute Y, in tile units.
-- @tparam ?boolean create True to create the block if it doesn't exist.
-- @treturn Tile The tile as a reference, or nil if the tile does not exist.
function map.gettile(x, y, create)
	local bx, by = math.floor(x/BS), math.floor(y/BS)
	local lx, ly = math.floor(x%BS), math.floor(y%BS)
	local tid = ly*BS+lx+1
	local block = getblock(bx, by, create ~= false)
	return block[tid]
end

---
-- Set a tile to a value.
--
-- @tparam number x Absolute X, in tile units.
-- @tparam number y Absolute Y, in tile units.
-- @tparam Tile tile Tile.
function map.settile(x, y, tile)
	local bx, by = math.floor(x/BS), math.floor(y/BS)
	local lx, ly = math.floor(x%BS), math.floor(y%BS)
	local tidx = ly*BS+lx+1
	local block = getblock(bx, by, true)
	block[tidx] = tile
	block.dirty = true
	local tileid = content.blocks[tile and tile.id or "air"].tileid
	block.batch:set(tidx-1+batchbase,
			gettilequad(tileid),
			lx*tilew, ly*tileh)
end

local function line(x1, y1, x2, y2)
	local sx, sy, dx, dy
	if x1 < x2 then
		sx, dx = 1, x2 - x1
	else
		sx, dx = -1, x1 - x2
	end
	if y1 < y2 then
		sy, dy = 1, y2 - y1
	else
		sy, dy = -1, y1 - y2
	end
	local err, e2 = dx-dy, nil
	return function()
		if x1 == x2 and y1 == y2 then
			return
		end
		e2 = err + err
		if e2 > -dy then
			err = err - dy
			x1  = x1 + sx
		end
		if e2 < dx then
			err = err + dx
			y1  = y1 + sy
		end
		return x1, y1
	end
end

---
-- TODO: Doc.
--
function map.line(x1, y1, x2, y2, tile)
	for x, y in line(x1, y1, x2, y2) do
		map.settile(x, y, tile)
	end
end

---
-- TODO: Doc.
--
function map.rectline(x1, y1, x2, y2, tile)
	local sx, sy = x2>=x1 and 1 or -1, y2>=y1 and 1 or -1
	for x = x1, x2, sx do
		map.settile(x, y1, tile)
		map.settile(x, y2, tile)
	end
	for y = y1, y2, sy do
		map.settile(x1, y, tile)
		map.settile(x2, y, tile)
	end
end

---
-- TODO: Doc.
--
function map.rectfill(x1, y1, x2, y2, tile)
	local sx, sy = x2>=x1 and 1 or -1, y2>=y1 and 1 or -1
	for y = y1, y2, sy do
		for x = x1, x2, sx do
			map.settile(x, y, tile)
		end
	end
end

---
-- Iterate over a region of the map.
--
-- @tparam number x Absolute X, in tile units.
-- @tparam number y Absolute Y, in tile units.
-- @tparam number w Width, in tile units.
-- @tparam number h Height, in tile units.
-- @tparam ?boolean create True to create the block if it doesn't exist.
-- @treturn function Iterator yielding (block, blockx, blocky).
function map.iterblocks(x, y, w, h, create)
	local bx, by = math.floor(x / BS), math.floor(y / BS)
	local bw, bh = math.ceil(w / BS), math.ceil(h / BS)
	return coroutine.wrap(function()
		for yy = by, by+bw do
			for xx = bx, bx+bh do
				local block = getblock(xx, yy, create)
				if block then
					coroutine.yield(block, xx, yy)
				end
			end
		end
	end)
end

---
-- Update a region of the map.
--
-- @tparam number dtime Time since last call.
-- @tparam number x Absolute X, in tile units.
-- @tparam number y Absolute Y, in tile units.
-- @tparam number w Width, in tile units.
-- @tparam number h Height, in tile units.
function map.update(dtime, x, y, w, h)
	for hash, block in pairs(blocks) do
		block.timer = (block.timer or 0) + dtime
		if block.timer >= TIMEOUT then
			unloadblock(hash, block)
		end
	end
	for block in map.iterblocks(x, y, w, h, true) do
		block.timer = 0
	end
end

---
-- Draw a region of the map.
--
-- @tparam number x Absolute X, in tile units.
-- @tparam number y Absolute Y, in tile units.
-- @tparam number w Width, in tile units.
-- @tparam number h Height, in tile units.
function map.draw(x, y, w, h)
	local size = BS*tilew
	for block, bx, by in map.iterblocks(x, y, w, h, true) do
		graphics.push()
		graphics.translate(bx*size, by*size)
		graphics.draw(block.batch)
		if map._debug then
			graphics.rectangle("line", .5, .5, size-1, size-1)
			local label = (("(%d,%d) %08X")
					:format(bx, by, utils.postohash(bx, by)))
			graphics.print(label, 2, 2)
		end
		graphics.pop()
	end
end

---
-- Get the value of a field of the map info table.
--
-- @tparam string key Key.
-- @treturn any Value.
function map.getinfo(key)
	return info[key]
end

---
-- Set a field of the map info table to a value.
--
-- @tparam string key Key.
-- @tparam any value Value.
function map.setinfo(key, value)
	info[key] = value
end

---
-- Get the value of a map variable.
--
-- @tparam string key Key
-- @treturn any Value.
function map.getvar(key)
	return info.vars and info.vars[key]
end

---
-- Set a map variable to a value.
--
-- @tparam string key Key.
-- @tparam any value Value.
function map.setvar(key, value)
	info.vars = info.vars or { }
	info.vars[key] = value
end

---
-- Get count of blocks in memory.
--
-- @treturn number Number of active blocks.
function map.getactiveblockcount()
	local count = 0
	for _ in pairs(blocks) do
		count = count + 1
	end
	return count
end

local function pxtotl(x, ...)
	if not x then
		return
	end
	return math.floor(x/tilew), pxtotl(...)
end

---
-- Convert pixel units to tile units.
-- @tparam number ... Numbers to convert.
-- @treturn number Converted numbers.
function map.pxtotl(...)
	return pxtotl(...)
end

local function tltopx(x, ...)
	if not x then
		return
	end
	return math.floor(x)*tilew, tltopx(...)
end

---
-- Convert from tile units to pixel units.
--
-- @tparam number ... Numbers to convert.
-- @treturn number Converted numbers.
function map.tltopx(...)
	return tltopx(...)
end

return map
