
local event = love.event
local filesystem = love.filesystem
local graphics = love.graphics
local keyboard = love.keyboard
local mouse = love.mouse
local system = love.system
local timer = love.timer
local window = love.window
local argparse = require "argparse"
local simpleui = require "simpleui"
local minser = require "minser"
local landtest = require "landtest"
local map = require "landtest.map"
local res = require "landtest.res"
local logging = require "landtest.logging"

local conf, userconf
local defconf = {
	map = "main",
	width = 800,
	height = 600,
	maximized = false,
	guiscale = 2,
	viewscale = 1,
	logfile = "@/landtest.log",
	config = "@/landtest.conf",
}

local savedir = filesystem.getSaveDirectory()

local root, topbar, bottombar
local viewx, viewy = 0, 0
local mousex, mousey = 0, 0
local tilew, tileh

local ctrl, alt, shift
local shiftstate = ""

map._debug = true

local function message(text)
	local ww, wh = window.getMode()
	local oldc = { graphics.getBackgroundColor() }
	graphics.setBackgroundColor(0, 0, 0)
	graphics.clear()
	graphics.origin()
	graphics.scale(conf.guiscale)
	local font = graphics.getFont()
	local tw, th = font:getWidth(text), font:getHeight(text)
	graphics.print(text,
			math.floor((ww/2-tw)/conf.guiscale),
			math.floor((wh/2-th)/conf.guiscale))
	graphics.present()
	graphics.setBackgroundColor(oldc)
end

local function drawrect(x, y, w, h, color)
	local lw = graphics.getLineWidth()
	local oldc = { graphics.getColor() }
	graphics.setColor(color)
	graphics.rectangle("line", x+lw/2, y+lw/2, w-lw, h-lw)
	graphics.setColor(oldc)
end

local function prettysize(n)
	if n >= 1048576 then
		return ("%.2f GB"):format(n/1048576)
	elseif n >= 1024 then
		return ("%.2f MB"):format(n/1024)
	else
		return ("%d KB"):format(n/1024)
	end
end

local function initui()
	root = simpleui.Widget { canfocus=true }

	topbar = root:addchild(simpleui.Box {
		mode = "h",
		simpleui.Label {
			text = "Hello, world!",
		},
	})

	bottombar = root:addchild(simpleui.Box {
		mode = "h",
		simpleui.Button {
			text = "Inventory",
		},
		simpleui.Button {
			text = "Notebook",
		},
		simpleui.Button {
			text = "Skills",
		},
		simpleui.Button {
			text = "SaveDir",
			activated = function()
				system.openURL("file:///"..savedir)
			end,
		},
	})

	function root:layout()
		topbar:rect(0, 0, 1, 1)
		local mw, mh = bottombar:minsize()
		bottombar:rect(0, root.h-mh, mw, mh)
	end

	local painttile
	local brushsize = 1

	function root:update(dtime)
		mousex, mousey = mouse.getPosition()
		mousex = math.floor((mousex-self.x)/conf.guiscale)
		mousey = math.floor((mousey-self.y)/conf.guiscale)
		mousex = math.floor((mousex+viewx)/tilew)
		mousey = math.floor((mousey+viewy)/tileh)
		if self.hasfocus then
			local dx = (keyboard.isDown("a") and -1
					or keyboard.isDown("d") and 1 or 0)
			local dy = (keyboard.isDown("w") and -1
					or keyboard.isDown("s") and 1 or 0)
			local mul = 1
			if ctrl then
				mul = mul * 10
			end
			if shift then
				mul = mul * 5
			end
			if alt then
				mul = mul * .1
			end
			viewx = viewx + dx*dtime*128*mul
			viewy = viewy + dy*dtime*128*mul
			if painttile then
				local x1, x2 = mousex-brushsize+1, mousex+brushsize-1
				local y1, y2 = mousey-brushsize+1, mousey+brushsize-1
				if shiftstate == "c" then
					map.rectline(x1, y1, x2, y2, painttile)
				elseif shiftstate == "" then
					map.rectfill(x1, y1, x2, y2, painttile)
				end
			end
		end
		topbar[1].text = ("(%d,%d)"):format(viewx, viewy)
		local vx, vy = math.floor(viewx), math.floor(viewy)
		map.update(dtime, math.floor(vx/tilew), math.floor(vy/tileh),
				math.ceil(root.w/tilew), math.ceil(root.h/tileh))
	end

	function root:mousepressed(x, y, b)
		painttile = (b == self.LMB and { id="dirt" }
				or b == self.RMB and { id="air" }
				or nil)
	end

	function root:mousereleased(x, y, b)
		painttile = nil
	end

	function root:wheelmoved(dx, dy)
		brushsize = math.max(1, math.min(25, brushsize+dy))
	end

	function root:paintbg()
		local vx, vy = math.floor(viewx), math.floor(viewy)
		graphics.push()
		graphics.translate(-vx, -vy)
		map.draw(math.floor(vx/tilew), math.floor(vy/tileh),
				math.ceil(root.w/tilew), math.ceil(root.h/tileh))
		drawrect(mousex*tilew, mousey*tileh,
				tilew, tileh,
				{ 255, 255, 255 })
		drawrect((mousex-brushsize+1)*tilew, (mousey-brushsize+1)*tileh,
				((brushsize-1)*2+1)*tilew, ((brushsize-1)*2+1)*tileh,
				{ 255, 255, 255, 128 })
		graphics.pop()
		graphics.push()
		graphics.translate(4, topbar.h+4)
		graphics.print("FPS: "..timer.getFPS()
				.."\nActive: "..map.getactiveblockcount()
				.."\nMem: "..prettysize(collectgarbage("count"))
				.."\nShift: "..(shiftstate or ""))
		graphics.pop()
	end

	root:setfocus()

	return root
end

local function updateshiftstate()
	shiftstate = table.concat({
		ctrl  and "c" or "",
		alt   and "a" or "",
		shift and "s" or "",
	})
end

local oldkeypressed = simpleui.keypressed
function simpleui.keypressed(key, scan, isrep)
	if key == "escape" then
		event.quit()
		return
	elseif key == "f2" and shiftstate == "" then
		collectgarbage()
	elseif key == "f2" and shiftstate == "c" then
		collectgarbage("restart")
	elseif key == "lshift" or key == "rshift" then
		shift = true
		updateshiftstate()
	elseif key == "lalt" or key == "ralt" then
		alt = true
		updateshiftstate()
	elseif key == "lctrl" or key == "rctrl" then
		ctrl = true
		updateshiftstate()
	end
	return oldkeypressed(key, scan, isrep)
end

local oldkeyreleased = simpleui.keyreleased
function simpleui.keyreleased(key, scan)
	if key == "lshift" or key == "rshift" then
		shift = nil
		updateshiftstate()
	elseif key == "lalt" or key == "ralt" then
		alt = nil
		updateshiftstate()
	elseif key == "lctrl" or key == "rctrl" then
		ctrl = nil
		updateshiftstate()
	end
	return oldkeyreleased(key, scan)
end

function love.errhand(err)
	logging.error(err, "\n", debug.traceback())
	logging.close()
	event.quit(1)
end

local function handlecmdline(argv)
	-- Hack to handle exit ourselves.
	-- We need this because `argparse` calls `os.exit` if called with
	-- `--help` as argument, even if we use `pparse`.
	local exitstatus = nil
	function os.exit(status) -- luacheck: ignore
		exitstatus = status or 0
		error("exit")
	end

	local parser = argparse(filesystem.getIdentity(), "2D sandbox game")

	parser:option("--map", "Set map name")
			:argname("NAME")
			:args(1)
	parser:option("--config", "Set config file")
			:argname("PATH")
			:args(1)
	parser:option("--log-file", "Set log file")
			:target("logfile")
			:argname("PATH")
			:args(1)
	parser:option("--width", "Set window width")
			:argname("NUMBER")
			:convert(tonumber)
			:args(1)
	parser:option("--height", "Set window height")
			:argname("NUMBER")
			:convert(tonumber)
			:args(1)
	parser:flag("--maximized", "Maximize window on start")
	parser:flag("--no-maximized", "Don't maximize window on start")
			:target("maximized")
			:action("store_false")
	parser:option("--gui-scale", "Set GUI scaling factor")
			:target("guiscale")
			:argname("NUMBER")
			:convert(tonumber)
			:args(1)
	parser:option("--view-scale", "Set view scaling factor")
			:target("viewscale")
			:argname("NUMBER")
			:convert(tonumber)
			:args(1)
	parser:flag("--open-save-dir", "Open save path in filer and exit")
			:target("opensavedir")
	parser:flag("--print-save-dir", "Print save path and exit")
			:target("printsavedir")

	local ok, c = pcall(function()
		return parser:parse(argv)
	end)

	if (not ok) or exitstatus then
		event.quit(exitstatus or 1)
		return
	end

	return c
end

local function readfile(filename)
	local file = io.open(filename)
	if not file then
		return
	end
	local data = file:read("*a")
	file:close()
	return data
end

local function loaduserconf(filename)
	local data = readfile((filename:gsub("^@", savedir)))
	if not data then
		return
	end
	local c = { }
	local func = assert(loadstring(data))
	setfenv(func, c)
	func()
	return c
end

local function writefile(filename, data)
	local file = assert(io.open(filename, "w"))
	assert(file:write(data))
	assert(file:close())
end

local function saveuserconf(filename)
	local t = { }
	for k in pairs(defconf) do
		local v = rawget(userconf, k)
		if v ~= nil and k ~= "config" then
			t[#t+1] = (("%s = %s")
					:format(k, minser.dump(v)))
		end
	end
	return writefile((filename:gsub("^@", savedir)),
			table.concat(t, "\n").."\n")
end

local function main(argv)
	-- Remove project path.
	table.remove(argv, 1)
	conf = handlecmdline(argv)
	if not conf then
		return
	end

	local conffile = conf.config or defconf.config
	userconf = loaduserconf(conffile) or { }
	userconf.config = nil

	setmetatable(userconf, { __index=defconf })
	setmetatable(conf, { __index=userconf })

	if conf.opensavedir then
		system.openURL("file:///"..savedir)
		event.quit(0)
		return
	elseif conf.printsavedir then
		io.stdout:write(savedir, "\n")
		event.quit(0)
		return
	end

	window.setMode(conf.width, conf.height, {
		resizable = true,
	})
	window.setTitle("LandTest")
	if conf.maximized then
		window.maximize()
	end

	graphics.setDefaultFilter("nearest", "nearest")
	graphics.setNewFont(8)

	logging.open((conf.logfile:gsub("^@", savedir)))
	logging.info("Session started.")

	message("Initializing...")

	local tileset = res.gettable("data/gfx/tileset.lua")
	tilew, tileh = tileset.tilewidth, tileset.tileheight

	landtest.dofile("data/lua/main.lua")

	map.open(conf.map)

	initui():run(conf.guiscale)

	message("Shutting down...")

	saveuserconf(conffile)

	map.close()

	logging.info("Session finished.")
	logging.close()
end

return main(arg)
