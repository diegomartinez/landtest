
---
-- TODO: Doc.
--
-- @module landtest
-- @see landtest.map

local filesystem = love.filesystem

local landtest = { }

landtest.logging = require "landtest.logging"
landtest.map = require "landtest.map"
landtest.res = require "landtest.res"

local stack = { push=table.insert, pop=table.remove }

---
-- TODO: Doc.
--
-- @tparam string filename
-- @tparam ?table env
function landtest.dofile(filename, ...)
	if not filename:match("^/") then
		local path = stack[#stack] or ""
		filename = path.."/"..filename
	end
	stack:push((filename:gsub("/[^/]+$", "")))
	local function bail(...)
		stack:pop()
		return ...
	end
	landtest.logging.verbose("Executing `"..filename.."`.")
	return bail(assert(filesystem.load(filename))(...))
end

return landtest
