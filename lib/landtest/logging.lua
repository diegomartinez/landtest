
---
-- TODO: Doc.
--
-- @module landtest.logging

local stringex = require "stringex"

local logging = { }

local logfile

---
-- Enums
-- @section enums

---
-- TODO: Doc.
--
-- @table LogLevel
-- @field error
-- @field warning
-- @field info
-- @field verbose

--- @section end

local levels = {
	error   = 1,
	warning = 2,
	info    = 3,
	verbose = 4,
}

---
-- TODO: Doc.
--
-- @tfield LogLevel level
logging.level = "verbose"

---
-- TODO: Doc.
--
-- @tparam ?string filename
function logging.open(filename)
	if logfile then
		logfile:close()
	end
	logfile = assert(io.open(filename, "a"))
end

---
-- TODO: Doc.
--
function logging.close()
	if logfile then
		logfile:close()
		logfile = nil
	end
end

local function write(...)
	if logfile then
		logfile:write(...)
		logfile:flush()
	end
	if io and io.stderr then
		io.stderr:write(...)
		io.stderr:flush()
	end
end

---
-- @tparam string category
-- @tparam any ...
function logging.print(category, ...)
	local n, t = select("#", ...), { ... }
	for i = 1, n do
		t[i] = tostring(t[i])
	end
	local date = os.date("%Y-%m-%d|%H:%M:%S")
	t = table.concat(t, "")
	for line in stringex.isplit(t, "\n") do
		write(date, "|", category, "|", line, "\n")
	end
end

local function makelogger(level)
	local category = level:upper()
	local l = levels[level]
	logging[level] = function(...)
		if l > levels[logging.level] then
			return
		end
		return logging.print(category, ...)
	end
end

---
-- TODO: Doc.
--
-- @function error
-- @tparam any ...
makelogger("error")

---
-- TODO: Doc.
--
-- @function warning
-- @tparam any ...
makelogger("warning")

---
-- TODO: Doc.
--
-- @function info
-- @tparam any ...
makelogger("info")

---
-- TODO: Doc.
--
-- @function debug
-- @tparam any ...
makelogger("verbose")

return logging
