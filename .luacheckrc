
self = false
unused_args = false

exclude_files = {
	"lib/argparse.lua",
	"lib/bresenham.lua",
	"lib/MessagePack.lua",
	"lib/klass.lua",
	"lib/minser.lua",
	"lib/path.lua",
	"lib/simpleui/",
	"lib/stringex.lua",
}

globals = {
	"love",
}

files["main.lua"] = {
	globals = { "print" },
}
