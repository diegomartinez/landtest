
# LandTest

A 2D sandbox game inspired by Terraria.

## License

This program is free software released under a permissive license. You can
use it both in Free and closed source software. See `LICENSE.md` for details.

## Features

## Requirements

* [LÖVE](http://love2d.org)
  (tested with 0.10.2, "Super Toast").
* [MessagePack](https://github.com/fperrad/lua-MessagePack)
* [argparse](https://github.com/mpeterv/argparse)
* [bresenham.lua](https://github.com/kikito/bresenham.lua)
* [klass](https://github.com/kaeza/lua-klass)
* [minser](https://github.com/kaeza/lua-minser)
* [path](https://github.com/kaeza/lua-path)
* [simpleui](https://github.com/kaeza/love-simpleui)
* [stringex](https://github.com/kaeza/lua-stringex)
* Optional:
    * [LuaSQLite3](http://lua.sqlite.org)
      (for `sqlite3` database backend)

## API Documentation

API documentation can be generated using using [LDoc][LDoc]. It is not
available online at the moment.

## User Manual

The user manual is available in the `doc` subdirectory.

[LDoc]: https://github.com/stevedonovan/LDoc
