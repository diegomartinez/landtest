
local content = require "landtest.map.content"

content.registerblock("air", {
	tileid = 0,
	name = "Air",
	solid = false,
	propagatelight = true,
	drop = "",
})

content.registerblock("dirt", {
	tileid = 1,
	name = "Dirt",
})

content.registerblock("grass", {
	tileid = 2,
	name = "Dirt",
	drop = "",
})

content.registerblock("stone", {
	tileid = 3,
	name = "Stone",
	drop = "",
})
