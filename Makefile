
APPNAME = landtest
APPVER = scm-0

.PHONY: default all love package luacheck run
.PHONY: docs docs-clean clean distclean

default: all

LOVE ?= love

LDOC ?= ldoc
LDOCFLAGS ?=

ZIP ?= zip -u -r
ZIPFLAGS ?= -9

RM ?= rm -f
RMDIR ?= rm -fr

MAINLOVE = $(APPNAME).love

MAINSRC = \
	lib/MessagePack.lua \
	lib/argparse.lua \
	lib/bresenham.lua \
	lib/klass.lua \
	lib/lovegfx.lua \
	lib/minser.lua \
	lib/path.lua \
	lib/stringex.lua \
	\
	lib/simpleui/Box.lua \
	lib/simpleui/Button.lua \
	lib/simpleui/Check.lua \
	lib/simpleui/Entry.lua \
	lib/simpleui/Image.lua \
	lib/simpleui/Label.lua \
	lib/simpleui/Option.lua \
	lib/simpleui/Slider.lua \
	lib/simpleui/Widget.lua \
	lib/simpleui/init.lua \
	\
	lib/landtest/map/db/null.lua \
	lib/landtest/map/db/lua.lua \
	lib/landtest/map/db/sqlite3.lua \
	lib/landtest/map/init.lua \
	lib/landtest/map/utils.lua \
	lib/landtest/logging.lua \
	lib/landtest/res.lua \
	lib/landtest/main.lua

MAINDATA = \
	data/gfx/tileset.png \
	data/gfx/tileset.lua

all: love

love: $(MAINLOVE)

$(MAINLOVE): $(MAINSRC) $(MAINDATA) main.lua conf.lua
	$(RM) $(MAINLOVE) && \
		$(ZIP) $(ZIPFLAGS) $(MAINLOVE) \
			$(MAINSRC) $(MAINDATA) main.lua conf.lua

docs: doc/api/index.html

doc/api/index.html: $(MAINSRC)
	cd doc && $(LDOC) -d api $(LDOCFLAGS) .

run: $(MAINLOVE)
	$(LOVE) $(MAINLOVE)

clean:

docs-clean:
	$(RMDIR) doc/api

distclean: clean docs-clean
	$(RM) $(MAINLOVE)
